from gurobipy import *
from datetime import datetime
from functions import *

def Solver(R,R_dist,speed,FC,I,D,V,
           Tdelta,lmbda,mu,timelimit,desired_gap,seedno,
           FCpairs,Ipairs,Dpairs,Vpairs,Vdict,labels,route,user_cuts):
    ### BEGIN CREATING THE SOLUTION PLOTTER ###
    def solution_plotter(R_dist,active,iteration):
        img = Image.open('map.png')
        px = img.load()
        BBox = ((-88.34, -87.54,
             41.72, 42.31))

        fig, ax = plt.subplots(figsize = (15,15))           #Adjusts figure size
        ax.set_xlim(BBox[0],BBox[1])                        #Adjusts x-axis to map
        ax.set_ylim(BBox[2],BBox[3])                        #Adjusts y-axis to map
        ax.axis('off')

        c_marker = 12; ABSM_marker = 10; FC_marker = 20       #Marker size settings
        r_scaled = 1/49.5*R_dist                            #Converts R into image scale
        
        ABSMs_used_cust_level = {d: [j for (i,j) in E if y[i,j,d].x>0.5 if j not in D] for d in D}
        ABSMs_used = list(set([item for sublist in list(ABSMs_used_cust_level.values()) 
                                             for item in sublist]))

        plt.plot(FCpairs[0],FCpairs[1], '.k', alpha =0.5,   #Add R label
             label = '$R$',markersize=FC_marker,
             markerfacecolor = 'none')
        unfilleddashedcircle(FCpairs[0],FCpairs[1],
             r_scaled,FC_marker)                            #Plot FC and add label
        
        
        plt.plot(Vpairs[ABSMs_used[0]][0],Vpairs[ABSMs_used[0]][1], 's',            #Add Used ABSM label
                     color = 'black',
                     label = '$\mathbf{Used~ABSM}$',
                     markersize=ABSM_marker)
        
        Idle = [j for j in I if j not in ABSMs_used]
        if len(Idle) > 0:
            plt.plot(Vpairs[Idle[0]][0],Vpairs[Idle[0]][1], 's',                        #Add Idle ABSM label
                         color = 'black',markerfacecolor='none',
                         label = '$\mathbf{Idle~ABSM}$',
                         markersize=ABSM_marker)
        
        plt.plot(Dpairs[0][0],Dpairs[0][1], '.',                                    #Add Customer label
                     color = "black",
                     label = '$\mathbf{Demand~point}$',
                     markersize=c_marker)
        
        if active == True:                                  #Add point labels if True
            for i in Vdict:
                plt.text(Vdict[i][0]*0.99999,
                         Vdict[i][1]*1.0003,
                         r'$\mathbf{%s}$'%(i),size=15)
        
        for i in Dpairs:        
            plt.plot(i[0],i[1], '.k',markersize=c_marker)                           #Plots Customers
                
        for j in I:
            if j not in ABSMs_used:
                plt.plot(Vdict[j][0], Vdict[j][1], 's',                             #Plot unused ABSMs
                    color='black',markerfacecolor='none',
                    markersize=ABSM_marker)
        for j in ABSMs_used:
            if j not in D:
                plt.plot(Vdict[j][0], Vdict[j][1], 's',                                 #Plot used ABSMs
                    color='black',
                    markersize=ABSM_marker)
        
        if route == True:
            np.random.seed(seedno)
            Routes = {d:[] for d in D}
            for (i,j) in E:
                for d in D:
                    if y[i,j,d].x>0.5:
                        Routes[d].append((i,j))
            colorlist = ['dimgray', 'midnightblue', 'indigo', 'darkmagenta',
                        'darkred','gold','darkorange','darkgreen','slategrey',
                         'saddlebrown','cyan', 'pink', 'purple','dodgerblue',
                         'mediumvioletred', 'crimson','tan', 'springgreen',
                        'maroon','teal','olive','yellow','fuchsia','rosybrown',
                        'peru','darkgoldenrod','navy','skyblue','darkkhaki',
                        'tomato']
            if len(D) > len(colorlist):
                colors = {d:'#%06X' % np.random.randint(0, 0xFFFFFF) for d in D}
            else:
                colors = dict(zip(D,colorlist[:len(D)]))

            for d in D:
                for (i,j) in Routes[d]:
                    plt.plot([Vdict[i][0],Vdict[j][0]],[Vdict[i][1],Vdict[j][1]], '--', 
                     color = colors[d], linewidth=3)

        plt.gca().axes.get_xaxis().set_visible(False)       #Disables x axis
        plt.gca().axes.get_yaxis().set_visible(False)       #Disables y axis

        plt.legend(loc='upper right', fancybox=True,        #Adds legend box
                   shadow=False, ncol=1, numpoints=1, 
                   fontsize = 20)
        plt.imshow(img, zorder=0, extent = BBox,            #Shows the figure
                  aspect= 'equal')
        plt.savefig('progress_iteration_{}.png'.format(iteration), dpi=600,
                    bbox_inches = 'tight',pad_inches = 0)
        plt.show()
    ### END CREATING THE SOLUTION PLOTTER ###

    ### BEGIN SOLUTION PRINTER ###
    def solutionprinter():
        for j in I:
            if Wq[j].x > 0.:
                print("Average queuing time at ABSM vertex {} is {}.".format(j,round(Wq[j].x,4)))
        print('\n\x1b[1;23;43m' +"The LB objective value is $%s"% str(m.ObjBound) + '\x1b[0m\n')
        print('\n\x1b[1;23;44m' +'The UB objective value is $%s'%str(UB_calculator()) +'\x1b[0m\n')
        print ('\n\x1b[1;23;45m' + 'The solution gap is currently %s'
               %(1-m.ObjBound/UB_calculator()) + '\x1b[0m\n')
        print(" ")
    ### END SOLUTION PRINTER ###

    ### BEGIN Wq CALCULATOR ###
    def Wqcalc(rhoval): #W calculator given rhoval
        return(rhoval/(2*mu*(1-rhoval)))
    ### END Wq CALCULATOR ###

    ### BEGIN UPPER BOUND T_HAT CALCULATOR ###
    def UB_calculator(): #Upper bound calculator
        T_hat = (sum(lmbda[d]*Tdelta[i,j]*y[i,j,d].x for (i,j) in E for d in D)
                   +sum(lmbda[d]*1/mu*y[i,j,d].x for (i,j) in E for d in D if j not in D)
                   +sum(lmbda[d]*Wqcalc(rho[j].x)*y[i,j,d].x for (i,j) in E for d in D if j not in D))
        return(T_hat)
    ### END UPPER BOUND T_HAT CALCULATOR ###

    ### BEGIN CALCULATING SOLUTION METRICS ###
    def solution_metrics_calculator():
        ABSMs_used_cust_level = {d: [j for (i,j) in E if y[i,j,d].x>0.5 if j not in D] for d in D}
        ABSMs_used = list(set([item for sublist in list(ABSMs_used_cust_level.values()) 
                                             for item in sublist]))
        rho_list = [rho[j].x for j in I if rho[j].x>0.0]
        Wq_list = [Wq[j].x for j in I if Wq[j].x>0.0]
        Avg_rho_j = (round(np.mean(rho_list),12) if len(rho_list)>0 else 0)
        Avg_Wq_j = (round(np.mean(Wq_list),12) if len(Wq_list)>0 else 0)
        T = m.ObjBound
        T_hat = UB_calculator()
        global solution_gap; solution_gap = 1-(T/T_hat)
        solution_time = (datetime.now()-starttime).total_seconds()
        Cor_avg_Wq_j = np.mean([Wqcalc(rho[j].x) for j in I if rho[j].x>0.0])
        return(ABSMs_used,Avg_rho_j, Cor_avg_Wq_j, T, T_hat,
               solution_gap, solution_time)
    ### END CALCULATING SOLUTION METRICS ###

    ### BEGIN PRINTING SOLUTION METRICS ###
    def table_printer():
        (ABSMs_used,Avg_rho_j, Cor_avg_Wq_j, T, T_hat,
               solution_gap, solution_time) = solution_metrics_calculator()
        table = PrettyTable(['seed', '# ABSMs',
                             'Avg. rho', 'Avg. Wq', 
                             'T', 'T_hat', 'Gap', 'Time'])
        table.add_row([seedno,
                       len(ABSMs_used) if len(ABSMs_used)>0 else "-",
                       round(Avg_rho_j,5) if Avg_rho_j>0 else "-",
                       round(Cor_avg_Wq_j,7) if Cor_avg_Wq_j>0 else "-",
                       '{:,.1f}'.format(round(T,2)), '{:,.1f}'.format(round(T_hat,2)),
                       round(solution_gap,6), round(solution_time,2)])
        print(table); print('\n\n')
    ### END PRINTING SOLUTION METRICS ###

    ### BEGIN (A,B) CALCULATOR ###
    def ab_calc(rhoval):
        rhosy = sy.symbols('rhosy', real=True)
        fWq = rhosy/(2*mu*(1-rhosy))
        d_Wq = sy.diff(fWq, rhosy).doit()
        B = (d_Wq.subs(rhosy, rhoval))
        A = (fWq.subs(rhosy, rhoval) - d_Wq.subs(rhosy, rhoval)*rhoval)
        return(float(A),float(B))
    ### END (A,B) CALCULATOR ###

    ### BEGIN DEFINING CUT CONSTRAINT GENERATOR ###
    def cut_cons_gen():
        cut_dict = {j: ab_calc(rho[j].x) for j in I if rho[j].x>min([2*lmbda[d]/mu for d in D])/2}
        for j in cut_dict.keys():
            for j_prime in I:
                m.addConstr(Wq[j_prime] >= cut_dict[j][0] + cut_dict[j][1]*rho[j_prime], 
                       name = "cut_cons{}".format(j,j_prime))
#                print("%s >= %s + %s * %s"%(Wq[j_prime], cut_dict[j][0], cut_dict[j][1], rho[j_prime]))
#                print("I have just added a new cut " +
#                       "based on a+b(rho)=%s+%s(rho) to location %s."%(cut_dict[j][0],
#                         cut_dict[j][1],j_prime))
        print("\nAt this iteration, we introduced %s cuts in total.\n"%(len(I)*len(cut_dict.keys())))
        return(print("Now beginning to solve the new program with added cuts!\n"))
    ### END DEFINING CUT CONSTRAINT GENERATOR ###

    def subI(set1,v2,R): #Subset of given sets
        return([v1 for v1 in set1             
                if(v1!=v2 and Tdelta[v1,v2]<=R)])
    E = [(i,j) for i in V for j in V          #Set of edges
         if(i!=j and Tdelta[i,j]<=R and i not in D and j not in FC)]
    ### END DEFINING PARAMETERS ###
    
    ### BEGIN CREATING THE MODEL ###
    m = Model("Drone-delivery-problem")
    starttime = datetime.now()
    ### END CREATING THE MODEL ###

    ### BEGIN INTRODUCING VARIABLES ###
    y, rho, Wq, beta = {}, {}, {}, {}
    for j in I:
        rho[j] = m.addVar(vtype=GRB.CONTINUOUS, ub = 0.9999, name="rho%s"%str([j]))
        Wq[j] = m.addVar(vtype=GRB.CONTINUOUS, name="Wq%s"%str([j]))
    for d in D:
        for (i,j) in E:
            y[i,j,d] = m.addVar(vtype=GRB.BINARY, name = "y%s"%str([i,j,d]))
            if j not in D:
                beta[i,j,d] = m.addVar(vtype=GRB.CONTINUOUS, name = "beta%s"%str([i,j,d]))
    ### END INTRODUCING VARIABLES ###

    ### BEGIN CREATING THE OBJECTIVE FUNCTION ###
    m.setObjective((quicksum(lmbda[d]*Tdelta[i,j]*y[i,j,d] for (i,j) in E for d in D)
                   +quicksum(lmbda[d]*1/mu*y[i,j,d] for (i,j) in E for d in D if j not in D)
                   +quicksum(lmbda[d]*beta[i,j,d] for (i,j) in E for d in D if j not in D)),
                     GRB.MINIMIZE)
    ### END CREATING THE OBJECTIVE FUNCTION ###

    ### BEGIN CREATING THE CONSTRAINTS ###
    for d in D:
        m.addConstr(quicksum(y[i,d,d] for i in I if Tdelta[i,d] <= R) == 1, 
                    name = "each_demand_is_met{}".format([d]))
        for i in I:
            if Tdelta[i,d] <= R:
                m.addConstr(2*Tdelta[i,d]*y[i,d,d] <= R, name = "drone_must_have_enough_battery{}".format([d]))
        for j in I:
            m.addConstr(quicksum(y[i,j,d] for i in subI(FC+I,j,R)) ==
                        quicksum(y[j,i,d] for i in subI(I+D,j,R)), 
                        name = "drone_in_vertex_drone_out_vertex{}".format([j,d]))
    for j in I:
        m.addConstr(rho[j] == 1/mu * quicksum(2*lmbda[d]*y[i,j,d] for i in subI(FC+I,j,R) for d in D),
                   name = "rhocons{}".format([j]))
    for (i,j) in E:
        for d in D:
            if j not in D:
                m.addQConstr(beta[i,j,d] >= Wq[j]*y[i,j,d])
    if len(user_cuts) > 0:
        ab_vals = [ab_calc(n) for n in user_cuts]
        for j in I:
            for (a,b) in ab_vals: 
                m.addConstr(Wq[j] >= a + b*rho[j], name = "user_cut{}".format([j,a,b]))
    ### END CREATING THE CONSTRAINTS ###

    ### BEGIN SOLVING ###
    print("Beginning to solve the problem!\n")
    m.setParam('TimeLimit', timelimit); #m.setParam('MIPgap', 0.000001)
    m.update(); m.optimize(); print("\n\n")
    
#     m.write("test.lp") # Writes the model in mathematical form (very important for detection)
#     if m.status == GRB.INFEASIBLE: # If infeasibility exists, it does following:
#         m.computeIIS()  # Computes Irreducible Incosistent Subsytems (Not Vital)
#         m.feasRelaxS(1, False, False, True) # Relaxes the problem so that it becomes feasible
#         m.optimize() # Give the optimize command once again!
#         m.printAttr('X', 'Art*') # Shows where the relaxation is done, so the problem can be resolved
    if m.status != GRB.INFEASIBLE and m.status!= GRB.INF_OR_UNBD and m.status!=GRB.UNBOUNDED:
        Remainingtime = timelimit-(datetime.now()-starttime).total_seconds()
        table_printer(); solution_plotter(R_dist,labels,0)
        LB, UB = {}, {}
        LB[0] = m.ObjBound; UB[0] = UB_calculator()
        ### END SOLVING ###

        ### BEGIN ADDING CUTS AND SOLVING ITERATIVELY ###
        cut_iter_gap =[solution_gap]; iteration = 1
        while solution_gap > desired_gap and Remainingtime > 0:
            starttimer = datetime.now()
            cut_cons_gen(); m.setParam('TimeLimit', Remainingtime)
            m.update; m.optimize(); print("\n\n")
            table_printer(); solutionprinter()
            solution_plotter(R_dist,labels,iteration)
            cut_iter_gap.append(solution_gap)
            Remainingtime -= (datetime.now()-starttimer).total_seconds()
            LB[iteration] = m.ObjBound; UB[iteration] = UB_calculator()
            iteration += 1
            if cut_iter_gap[-1] == cut_iter_gap[-2]:
                break
        ### END ADDING CUTS AND SOLVING ITERATIVELY ###
        ysol = {(i,j,d):y[i,j,d].x for (i,j) in E for d in D}
        rhosol = {j: rho[j].x for j in I}
        Wqsol = {j: Wq[j].x for j in I}
        comptime = timelimit-Remainingtime
        return(ysol,rhosol,Wqsol,LB,UB,comptime)
    else:
        return([],[],[],[],[],[])