# Delivery Drone Route Planning over a Battery Swapping Network
------------------

This project develops an optimization model to solve the
delivery drone route planning problem. We share the optimization
code and the data files
developed at Argonne National Laboratory.

This code and data files are supplementary materials for the manuscript 
"Delivery Drone Route Planning over a Battery Swapping Network"
by Taner Cokyasar.

Authors and Contributors
--------------------------
Copyright (C) Taner Cokyasar - All Rights Reserved

Unauthorized copying of this file, via any medium is strictly prohibited

Proprietary and confidential

Written by Taner Cokyasar <tcokyasar@anl.gov>, October 2020

System Requirements
-------------------------
Python 3.7.3+

Gurobi 9.0.2+

Running
------------------------
To run, gather main.ipynb, functions.py, map.py, points.xlsx, and solver.py in the
same directory or change the directories as needed. You may run "main.ipynb" to solve different
instances and access all the parameters and variables within this file. This file collects
location and population data from "points.xlsx" and a map from "map.py" for visualization purposes.
It also calls "functions.py" to generate the problem map and create the problem instance for the given
parameter settings. The "solver.py" file is called by "main.ipynb" to solve the problem. This file
includes the Gurobi solver coding and the solution visualization code which generate maps and tables based 
on the solver output. For smooth-visualization, we suggest using the Jupyter Notebook through Anaconda
interface. The solution maps for each iteration are also saved to the directory during the run.
